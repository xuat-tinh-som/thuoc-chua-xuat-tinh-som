Trong y học cổ truyền, xuất tinh sớm còn được gọi là chứng tảo tiết, do nhiều nguyên nhân khác nhau như về tâm lý, bẩm sinh… Việc điều trị xuất tinh sớm có rất nhiều phương pháp khác nhau trên nhiều phương diện của y học, trong đó có Đông y.
Một số bài thuốc thông dụng để điều trị có thể tham khảo: 
http://yhoccotruyensaigon.com
Bài thuốc 1 

Ðông trùng hạ thảo 10g
Nhân sâm 8g
Hoài sơn 30g
Nhung hươu 150g
Đại táo 15g

Sơ chế: Nhung hươu rửa sạch, sau đó đem thái mỏng. Thái phiến nhân sâm. 
Chế thuốc: 
http://yhoccotruyensaigon.com/chi-phi-chua-va-dieu-tri-benh-xuat-tinh-som-het-bao-nhieu-tien-515.html
Rửa sạch các vị thuốc cho bớt đi bụi vụn. Cho tất cả các vị thuốc vào nồi và hầm bằng lửa nhỏ trong khoảng 2,5 - 3 giờ. Sau đó, nêm các gia vị trong nấu ăn sao cho vừa miệng. Chia đều vài phần và ăn hết trong ngày.
Công dụng: Tốt cho thận, điều trị được các triệu chứng yếu sinh lý như xuất tinh sớm, tinh dịch lỏng, suy giảm ham muốn tình dục, hay đi tiểu đêm, tiểu đường, suy nhược thần kinh...


>> chi phí điều trị xuất tinh sớm
Bài thuốc 2 

Thục địa 30g
Tỏa dương 20g
Đỗ trọng 30g
Đuôi lợn 150g
Gừng tươi 15g
Đại táo 8 quả

Sơ chế: 
http://yhoccotruyensaigon.com/chua-benh-xuat-tinh-som-khong-dung-thuoc-co-duoc-khong-312.html
Cạo bỏ lông ở đuôi lợn và rửa thật sạch, sau đó chặt từng đoạn.
Gừng tươi giã nhuyễn.
Các vị thước rửa sơ để sạch bụi.

Nấu thuốc: Cho đuôi lợn, gừng và các vị thuốc vào nồi. Hầm với lửa nhỏ khoảng 2,5-3 tiếng đồng hồ rồi nêm gia vị sao cho vừa ăn. Chia đều ăn hết trong ngày. 
Công dụng: Giúp điều trị các triệu chứng yếu sinh lý như xuất tinh sớm, suy giảm ham muốn tình dục, tinh dịch ít và loãng, cơ thể hao gầy, tóc rụng nhiều, u phì đại tiền liệt tuyến mạn tính, suy giảm công năng tuyến thượng thận...
Kết hợp uống các bài thuốc chữa xuất tinh sớm với các bài tập trị liệu sẽ đem lại kết quả tốt nhất cho người bị bệnh này:

Bài tập 1: Tập trung vào cảm giác của bộ phận sinh dục 
Bài tập chống xuất tinh sớm này cần đến thủ dâm. Bạn có thể tập cho mình cách điều khiển và tạo cảm giác hưng phấn nhờ thủ dâm. Trước tiên, nam giới cần chọn cho mình một không gian thoải mái, kín đáo để tự nhiên nhất theo cách của mình. Bạn cũng tạo cho mình tư thế đúng, nằm, ngồi sao cho thuận tiện và thoải mái nhất để thực hiện các động tác. 
Bạn cầm toàn bộ dương vật hay đầu dương vật, kích thích theo nhịp. Kích thích nhẹ từ từ hoặc mạnh. Sau đó làm chậm lại và cố gắng tập trung vào khoái cảm xuất phát của dương vật. Bạn tập trung vào cảm giác tạo cực khoái trước thời điểm xuất tinh và thả lỏng sau đó để cảm nhận độ cực khoái. Lưu ý, với bài tập trị xuất tinh sớm tại nhà này bạn không nên lạm dụng thủ dâm nhé. 
http://yhoccotruyensaigon.com/nhung-bai-thuoc-nam-chua-va-tri-xuat-tinh-som-hieu-qua-nhat-158.html
Bài tập 2: Kích thích dương vật theo phương pháp “ngừng – bắt đầu” 
>> chữa xuất tinh sớm không dùng thuốc Với bài tập chữa xuất tinh sớm này bạn có thể tự luyện tập một mình tại nhà bằng cách thủ dâm hoặc thực hiện khi quan hệ tình dục. 
Nếu thủ dâm, bạn có thể thực hiện giãn cách so với bài tập 1 từ 2 – 3 ngày. Khi dùng tay kích thích dương vật khi đạt hưng phấn gần xuất tinh thì bạn ngưng lại vài giây để giảm hưng phấn rồi tiếp tục trở lại. Bạn có thể lặp đi lặp lại vài lần sau đó. Lưu ý là không nên ngưng kích thích quá lâu vì có thể làm mất cương cứng. 
Khi quan hệ tình dục, bạn cũng thực hiện những chuyển động nhịp nhàng trong âm vật và khi nhận thấy có hiện tượng chuẩn bị xuất tinh thì ngưng lại vài giây như trên để giảm hưng phấn, sau đó kích thích trở lại. Làm như vậy cho tới khi bạn cảm thấy đủ thỏa mãn thì có thể xuất tinh theo ý muốn.